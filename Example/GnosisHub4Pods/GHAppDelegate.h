//
//  GHAppDelegate.h
//  GnosisHub4Pods
//
//  Created by barcodepandora on 10/30/2015.
//  Copyright (c) 2015 barcodepandora. All rights reserved.
//

@import UIKit;

@interface GHAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
