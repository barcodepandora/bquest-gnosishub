//
//  main.m
//  GnosisHub4Pods
//
//  Created by barcodepandora on 10/30/2015.
//  Copyright (c) 2015 barcodepandora. All rights reserved.
//

@import UIKit;
#import "GHAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([GHAppDelegate class]));
    }
}
