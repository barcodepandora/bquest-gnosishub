# GnosisHub4Pods

[![CI Status](http://img.shields.io/travis/barcodepandora/GnosisHub4Pods.svg?style=flat)](https://travis-ci.org/barcodepandora/GnosisHub4Pods)
[![Version](https://img.shields.io/cocoapods/v/GnosisHub4Pods.svg?style=flat)](http://cocoapods.org/pods/GnosisHub4Pods)
[![License](https://img.shields.io/cocoapods/l/GnosisHub4Pods.svg?style=flat)](http://cocoapods.org/pods/GnosisHub4Pods)
[![Platform](https://img.shields.io/cocoapods/p/GnosisHub4Pods.svg?style=flat)](http://cocoapods.org/pods/GnosisHub4Pods)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

GnosisHub4Pods is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "GnosisHub4Pods"
```

## Author

barcodepandora, barcodepandora@gmail.com

## License

GnosisHub4Pods is available under the MIT license. See the LICENSE file for more info.
